FROM registry.gitlab.com/eidheim/docker-images:arch

COPY http_examples /usr/local/bin

CMD ["/usr/local/bin/http_examples"]